---
layout: page
title: About
permalink: /about/
---
We are developers who love open-source and free software. 
[Clean code](https://clean-code-developer.com/) is more than using patterns and providing unit tests. Nevertheless, it greatly helps to know about [PEP 20](https://www.python.org/dev/peps/pep-0020/#the-zen-of-python) and [SOLID principles](https://en.wikipedia.org/wiki/SOLID), and writing clean unit tests is the foundation of [agile development](https://en.wikipedia.org/wiki/Agile_software_development#Overview). As developers we need to share code freely to improve our skills. Therefore, great code should always be open-source and available for everyone to improve. *The shorter our code get, the more skilled we become and the [less our software sucks](https://suckless.org/philosophy/).* 

But that is just code. We provide it freely for anyone to adapt.
Our target is to provide great features with the minimum of code. Check out our [GitLab space](https://gitlab.com/just-code) and contribute.

## imprint

Responsible for the content of this webpage is:

Adam Furmanczuk  
Moosburgstrasse 17  
8307 Effretikon  
Schweiz
