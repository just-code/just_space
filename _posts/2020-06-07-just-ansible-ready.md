---
layout: post
title:  "just-code with ansible"
date:   2020-06-07 15:54:00 +0200
categories: jekyll update
---
Ansible is great for managing your server installations. It takes a while to get familiar with the syntax, but in the long run it will save you a lot of time when installing and updating software on your remote servers. If you are new to ansible, there is a [nice video](https://www.ansible.com/resources/videos/quick-start-video).

We Want to make life *just* simpler for you. So when you decide to use our open source contact form, we worked hard to provide you an [ansible playbook](https://gitlab.com/just-code/just_ansible). This automation will setup nginx with PHP7, acquire a free HTTPS certificate with [cerbot](https://certbot.eff.org/about/) for your domain and deploy and install the [contact backend](https://gitlab.com/just-code/just_contact_backend) to your new server.

