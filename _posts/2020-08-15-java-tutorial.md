---
layout: post
title:  "How to become an enterprise developer"
date:   2020-08-15 09:15:00 +0200
categories: java springboot maven docker
---
It takes years of practice to become a good java developer. There is a list of things you need to be familiar with:

* Maven (and Gradle) to mange a Java project.
* Unit testing is crucial when programming in any language.
* Springboot is a framework enables you to deploy Java Code as a web service.
* Docker, because  nowadays everything is dockerized and companies like the idea of software that runs in an isolated container

I work since 2011 as a Java and python developer, and totally agree that [professionalism is awareness and principles](https://clean-code-developer.com/). Among other things, awareness includes knowledge about standard tools and techniques. Based on my experience, an important set of tools is maven, springboot, unit tests and docker.

There is a ton of excellent material on [udemy](https://www.udemy.com/) on all of those tools and techniques. Following along the exercises gives you understanding and enables you to use them as well. However, to just give you an example how use all of them in one project I created a [tutorial](https://gitlab.com/just-code/swiss_tax).

