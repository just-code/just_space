---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---
![Just Code Logo](/images/just_code_plain.svg)

Just-code is a team of developers convinced that providing and using open-source solutions enable the developer community be more productive and perform better services for society. On this website we show an overview of our open-source projects:

* [just contact form](https://www.just-code.io). The code is availabe on GitLab. Checkout [backend](https://gitlab.com/just-code/just_contact_backend) and [frontend](https://gitlab.com/just-code/just_contact_frontend)
* [Jekyll theme](https://gitlab.com/just-code/just_jekyll) based on [HTML5UP Photon](https://html5up.net/photon) 
*  [ansible playbook](https://gitlab.com/just-code/just_ansible) to install the contact backend to a new server
* [swiss tax](https://gitlab.com/just-code/swiss_tax) a Java, maven, springboot, unit testing and docker tutorial for programmers with basic Java skills

The project started May 2020, so stay tuned for more useful code to come. Your collaboration [with us on GitLab](https://gitlab.com/just-code/) is highly encouraged.
